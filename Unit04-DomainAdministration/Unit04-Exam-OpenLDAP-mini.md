---
title: \[OpenLDAP\] - OpenLDAP Exam
subtitle: "Unit 04"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Introduction

Read the exam carefully, and answer all the questions possible. Do not STUCK, if one step not works, 
continue with the rest of the exam. Everything was defined to permit this.

\newpage

# Task 01

Create a new CN in LDAP: `cn=trasgos`, and  create 5 users in this cn:

* trasgo01
* trasgo02
* trasgo03
* trasgo04
* trasgo05

The UID must be from 15001 (for trasgo01).

Take an snapshot of the all users created, upload to Aules.

# Task 02

Using `slapcat` or `ldapsearch`, create an LDIF file with all the users in the `cn=trasgos`.
Modify the LDIF in order to create 5 users more, from trasgo06 to trasgo07. 

Upload the LDIF to Aules.

# Task 03

Using `ldapmodify`, add the users to your OpenLDAP and make an screenshot of the command output of `ldapmodify`.


\newpage
# Task 04

Change your `sssd.conf` in order to allow the users for your new `cn=trasgos`, here is an example.

```shell
[sssd]
services = nss, pam, ifp
config_file_version = 2
domains = smx2023.net

[nss]
filter_groups = root
filter_users = root
reconnection_retries = 3

[domain/smx2023.net]
ldap_id_use_start_tls = True
cache_credentials = True
ldap_search_base = dc=ubuntusrv, dc=smx2023,dc=net
id_provider = ldap
debug_level = 3
auth_provider = ldap
chpass_provider = ldap
access_provider = ldap
ldap_schema = rfc2307
ldap_uri = ldap://ubuntusrv.smx2023.net
ldap_default_bind_dn = cn=admin,dc=ubuntusrv,dc=smx2023,dc=net
ldap_id_use_start_tls = true
ldap_default_authtok = Lin4dm1n
ldap_tls_reqcert = demand
ldap_tls_cacert = /etc/ssl/certs/ldapcacert.crt
ldap_tls_cacertdir = /etc/ssl/certs
ldap_search_timeout = 50
ldap_network_timeout = 60
ldap_access_order = filter
ldap_access_filter = (objectClass=posixAccount)
ldap_user_search_base = cn=trasgos,dc=ubuntusrv,dc=smx2023,dc=net
ldap_user_object_class = inetOrgPerson
ldap_user_gecos = cn
enumerate = True
debug_level = 0x3ff0
```

Make an screenshot of the graphical login for a user from `cn=trasgos`.

\newpage
# Task 05 

*WARNING:*

If you can't add the users using ldapmodify and the LDIF, create them manually and make the next part:

Configure the server and then make a *Shell Script* and set up **at the graphical login of the user**. (`xdg-freedesktop`), that makes
the next checks and operations:

- Test if the user belongs to the `cn=trasgos`.
- If the users belongs to that cn, then:
	- Mount a custom directory from the server (**nfs**).
	- For this step, you must configure an alias for the 
	  server at the bind9, or using hosts: **ldapexam.smx2023.net** and this name 
	  must be used at the client in the configuration file.
	- The path (at the server) must be: `/srv/nfs/exam-ldap-nfs/`.
	- The path (at the client) must be: `$HOME/shared/smx2023.net/exam-ldap-nfs/`.
- If the user id is an even number, then:
	- Mount a custom directory from the server (**smb**).
	- Use the previous alias: **ldapexam.smx2023.net**.
	- The path (at the server) must be: `/srv/samba/exam-ldap-smb/`.
	- The path (at the client) must be: `$HOME/shared/smx2023.net/exam-ldap-smb/`.

The script must make log at the syslog for every step and result. If the `PATH`(s) not exists, must be created (at the client).

