#!/usr/bin/make -f

#TEMPLATE_TEX_PD="rsrc/templates/pd-nologo-tpl.latex"
# Colors
BLUE= \e[1;34m
LIGHTBLUE= \e[94m
LIGHTGREEN= \e[92m
LIGHTYELLOW= \e[93m

RESET= \e[0m

# Templates 
TEMPLATE_TEX_PD="../rsrc/templates/eisvogel.tex"
PANDOC_OPTIONS="-V fontsize=12pt -V mainfont="Ubuntu" --pdf-engine=xelatex "
TEMPLATE_TEX_TASK="../rsrc/templates/eisvogel.tex"

# PDFS
PDF_PATH:=$(shell readlink -f PDFS)

# Units 
UNIT01_DIR:=$(shell readlink -f Unit01-Introduction-and-Virtualization/)
UD01_FILES := $(wildcard $(UNIT01_DIR)/*.md)

UNIT02_DIR:=$(shell readlink -f Unit02-Software-and-Updates)
UD02_FILES:=$(wildcard $(UNIT02_DIR)/*.md)

UNIT03_DIR:=$(shell readlink -f Unit03-FileSystems)
UD03_FILES:=$(wildcard $(UNIT03_DIR)/*.md)

UNIT04_DIR:=$(shell readlink -f Unit04-DomainAdministration)
UD04_FILES:=$(wildcard $(UNIT04_DIR)/*.md)

UNIT05_DIR:=$(shell readlink -f Unit05-SharingResources)
UD05_FILES:=$(wildcard $(UNIT05_DIR)/*.md)

UNIT0X_DIR:=$(shell readlink -f Unit0X-Resources)
UD0X_FILES:=$(wildcard $(UNIT0X_DIR)/*.md)

UNIT0Z_DIR:=$(shell readlink -f Unit0Z-Recover)
UD0Z_FILES:=$(wildcard $(UNIT0Z_DIR)/*.md)

PEAC_DIR:=$(shell readlink -f PEAC)
PEAC_FILES:=$(wildcard $(PEAC_DIR)/*.md)

# RULES

clean:
	@echo " [${BLUE} * Step : Clean ${RESET}] "
	@echo "${LIGHTBLUE} -- PDFS ${RESET}"
	rm -f PDFS/*.pdf
	rm -f PDFS/*.odt


files:
	@echo " [${BLUE} * Step : Files ${RESET}] "
	@echo "${LIGHTBLUE} * Creating folder [ PDFS ]${RESET}"
	mkdir -p PDFS

prog-didactica: files
	@echo " [ Step : prog-didactica ]"
	@echo " * [ PDF ] : Programacion Didactica ..."
	
	@cd ProgramacionDidactica/ && pandoc --template $(TEMPLATE_TEX_PD) $(PANDOC_OPTIONS) -o $(PDF_PATH)/ProgramacionDidactica_SOX.pdf ./PD_*.md

	@echo " * [ ODT ] : Programacion Didactica ..."
	@cd ProgramacionDidactica/ && pandoc -o $(PDF_PATH)/ProgramacionDidactica_SOX.odt ./PD_*.md 
	
	@echo " * [ PDF Result ] : $(PDF_PATH)/ProgramacionDidactica_SOX.pdf"
	evince $(PDF_PATH)/ProgramacionDidactica_SOX.pdf

unit-01: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} UD 01 - Introduction and Virtualization ${RESET}] "

	@for f in $(UD01_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT01_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done

unit-02: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 02 - Software and Updates ${RESET}] "

	@for f in $(UD02_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT02_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	

unit-03: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 03 - Software and Updates ${RESET}] "

	@for f in $(UD03_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT03_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	


unit-04: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 06 - OpenLDAP ${RESET}] "

	@for f in $(UD04_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT04_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    	done	

unit-05: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 07 - Nagios and Beyond ${RESET}] "
	@for f in $(UD05_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT05_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done	

peac: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} PEAC ${RESET}] "

	@for f in $(PEAC_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(PEAC_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done



unit-0z: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 0Z - Recover 2022 ${RESET}] "

	@for f in $(UD0Z_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT0Z_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done	



unit-0x: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 03 - Software and Updates ${RESET}] "

	@for f in $(UD0X_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT0X_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	
