\newpage 

# Unidad 02 : Software y Actualizaciones

## Temporalización

40 Sesiones

## Contenidos

* Características y Licencias de Software.
* Seguridad y Actualizaciones.
* Instalación en GNU/LinuX.
* Instalación mediante .*msi*.
* Markets.
* La línea de comandos - Repaso.
* PowerShell
* BASH

## Actividades de Enseñanza-Aprendizaje

* Instalación de un Software desde las Fuentes.
* Instalación de paquetes específicos.
* Configuración de paqueteria.
* Ficheros de configuración (*globales y locales*).
* Presentación de los diferentes modos de trabajo.
* Creación de pequeños Scripts de Shell.
* Corregir scripts de Shell que contienen fallos.
