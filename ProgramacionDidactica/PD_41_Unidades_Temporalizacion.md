\newpage

# Temporalización 

| Unidades | Sesiones | Evaluación | Peso  en la *Evaluación* |
|:--------| :-: | -: | -: |
| Unidad 01 : Introducción y Virtualización | 10 |  1ª EVA | 10 %| 
| Unidad 02 : Software y Actualizaciones| 40 | 1ª EVA | 40 %|
| Unidad 03 : Sistemas de Ficheros y Permisos | 30 | 1ª EVA | 50 %|
|||| ***1ª** EVA* **(30%)**||
| Unidad 04 : Administración del Dominio| 50 | 2ª EVA | 50 %| 
| Unidad 05 : Recursos Compartidos| 40 | 2ª EVA | 50 %| 
|||| ***2ª** EVA* **(70%)**|
