\newpage 
# Unidad 04 : Administración del Dominio

## Temporalización

50 Sesiones

## Contenidos

* Conceptos previos.
* Servicios de directorio.
* LDAP.
* Dominios.
* Bosque.
* Active Directory.
* Administración de Cuentas, Usuarios, Grupos y Equipos.
* Grupos de Seguridad.
* Ámbito de los grupos.
* Perfiles Móviles.
* Redirección de carpetas.
* Infraestructura de la Red.
* Servicios del Dominio.
* Unión al Dominio desde Windows.
* Políticas de Grupo.
* Unión al Dominio desde GNU/LinuX.
* Clientes de LDAP.
* Otras Arquitecturas y Clientes.

## Actividades de Enseñanza-Aprendizaje

* Crear un Dominio en Windows 2019 Server.
* Crear usuarios y usuarios plantilla estableciendo configuraciones comunes.
* Clonar usuarios a través de herramientas gráficas y PowerShell.
* Crear Grupos de Seguridad.
* Instalar y Configurar un OpenLDAP.
* Instalar el gestor phpLDAPAdmin y configurarlo para administrar el LDAP.
* Crear usuarios, grupos, en OpenLDAP.
* Planificar la Red y realizar un esquema.
* Unir al Dominio una máquina con Windows.
* Implementar una GPO que aplique varias configuraciones en los usuarios y máquinas del Dominio.
* Unir al Dominio una máquina GNU/LinuX.
* Permitir el inicio de sesión de usuarios de LDAP en una máquina GNU/LinuX.
