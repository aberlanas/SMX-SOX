\newpage

# Unidades didácticas

A continuación se presentan las unidades didácticas de la programación.

La estructuración en Unidades Didácticas ha de entenderse siempre de
la manera más flexible posible, pues el ritmo de desarrollo y su aplicación en el
aula dependerá de aspectos como la evaluación inicial del alumno, su
concreción respecto a las actividades de enseñanza-aprendizaje, etc.
La coordinación con el departamento y el equipo educativo también
resulta beneficiosa, ya que podemos trabajar conceptos desde varios módulos,
fomentando de esta manera su aprendizaje.

## Listado de Unidades

* Unidad 01 : Introducción y Virtualización.
* Unidad 02 : Software y Actualizaciones.
* Unidad 03 : Sistemas de Ficheros y Permisos.
* Unidad 04 : Administración del Dominio.
* Unidad 05 : Recursos Compartidos.

