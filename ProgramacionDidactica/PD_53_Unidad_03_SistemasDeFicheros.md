\newpage 

# Unidad 03 Sistemas de Ficheros y Permisos

## Temporalización

30 Sesiones

## Contenidos

* Repaso Sistemas de Ficheros.
* Carpetas de Software y de SO en Windows.
* Carpetas de Usuarios en Windows.
* Estructura del árbol de directorios en Sistemas GNU/LinuX.
* Carpetas de Usuarios en GNU/LinuX.
* Definición de Usuarios y Grupos.
* Usuarios y grupos predefinidos en Sistemas Windows.
* Permisos y Control de Acceso. Operaciones básicas.
* Usuarios y grupos predefinidos en Sistemas GNU/LinuX.
* Administración de usuarios y grupos en Sistemas GNU/LinuX.


## Actividades de Enseñanza-Aprendizaje

* Conocer y diferenciar los diferentes sistemas de ficheros.
* Gestion de archivos y carpetas mediante PowerShell y BASH.
* Perfiles móviles.
* Carpetas relevantes para las configuraciones.
* Crear usuarios locales en Windows 10.
* Gestionar directivas de cuentas para proteger el sistema.
* Crear usuarios desde PowerShell usando variables.
* Administrar grupos locales desde PowerShell.
* Modificar los permisos de acceso a determinadas carpetas.
* Proyecto de Atriles Virtuales.
