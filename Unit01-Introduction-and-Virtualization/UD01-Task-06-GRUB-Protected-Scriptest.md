---
title: Unit 01 - TaskProject GRUB and Pass - Scriptest
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# TaskProject: GRUB and Password - Scrip for testing

Code a Shell script that makes the next checks programatically:


1. Test if a *dualboot* is present (With Windows), if not present: Display an error message and exit with error.
2. Test if `grub-pc` is installed, if not installed: Display an error message and exit with error.
3. Test if the default user : *transistor* and password *transistor* are set in the correct file:

```shell
/etc/grub.d/00_header
```

   if not, display an error message an exit with error.

4. Test if all the other entries has the correct parameters in order to provide the correct behaviour to the
GRUB when it starts.  For each test , display a message that shows if the entrie is correct or display an error message and
exit with error.

Examples of execution:

```shell
user@machine:~$sudo ./grub-tester.sh
- Test if is a dualboot : Ok
- Test if grub-pc is installed : Ok
- Test if /etc/grub.d/00_header exists : Ok
- Test if /etc/grub.d/00_header has the user and pass: NO - ERROR
```

```shell
user@machine:~$sudo ./grub-tester.sh
- Test if is a dualboot : Ok
- Test if grub-pc is installed : Ok
- Test if /etc/grub.d/00_header exists : Ok
- Test if /etc/grub.d/00_header has the user and pass: Ok
- Test if the parameter is set in the first menuentry: Ok
- Test if the parameter is set in the second menuentry: Ok
- Test if the parameter is set in the third menuentry: Ok
- Everything is fine
```

```shell
user@machine:~$sudo ./grub-tester.sh
- Test if is a dualboot : Ok
- Test if grub-pc is installed : NO - ERROR
```

