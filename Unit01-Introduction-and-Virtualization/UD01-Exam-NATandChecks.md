---
title: Unit 02 - Exam - NAT and Checks
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# Task


In the next tasks you must install services in the virtualmachine and must be checked from the host machine.

The script and the tests must be executed from the "Real Machine".

Using ShellScript, PowerShell or Python create several Scripts in order to keep the mental sanity when you are working with VirtualBox machines and 
diferent configurations.

The score of the exam is the next:

The basic checks scores : 1 point.

- First Additional check correctly done: until 2 points.
- Second Additional check correctly done : until 3 points.
- Third Additional check correctly done: until 4 points.

## Basic checks (1 points).

1. List the names of the all the VBoxMachines configured.
2. For each of them, list the status and the network conectivity for each network adapter.
3. For each NatNetwork configured in the VirtualBox list the NatTable with port forwarding before the next exercises.

\newpage
## SSH service test

Install openssh (port 22) in the virtualmachine and create a NAT Portforwarding Rule to use the Host port **4222**. 

Test the ssh from your host machine (windows has sshclient too, no problem).

Re-execute the step 3 from the basic checks and verify that the NatNetwork is modified in the correct way.

Modify the checkScript in order to add a "SSH Verify" NatRule, that shows if the Guest has the port 22 is redirected to the Host 4222 and 
display a message.


## Apache service test

Install apache2 (port 80) in the virtualmachine and create a NAT Portforwarding Rule to use the Host port **4280**. 

Test the Apache from your Host machine using Firefox:

**http:\\\\YOUR_HOST_IP_HERE:4280**

Re-execute the step 3 from the basic checks and verify that the NatNetwork is modified in the correct way.

Modify the checkScript in order to add a "Apache2 Verify" NatRule, that shows if the Guest has the port 80 is redirected to the Host 4280 and 
display a message.

## Example 

The first 2 checks must be in a separated script named:

```shell
basicVBoxCheck.sh
```

The last check ( point 3), must be in a separated script: 

```shell
basicVBoxNATCheck.sh
```


\newpage
## SSH service test

Install openssh (port 22) in the virtualmachine and create a NAT Portforwarding Rule to use the Host port **XX22**. 

If you are wondering about the host port, is easy to find it!, it is explained in the next paragraph:

```Interlude
Ayer, durante la charla de Autonomos, 
se dijo cual era la tasa de Autonomos 
que se tendría que pagar hasta Enero.
Esa cifra, concatenada al puerto 22 
es el puerto del anfitrion al que 
se debe redirigir el tráfico del SSH de la máquina invitada.
```

Continue...

Test the ssh from your host machine (windows has sshclient too, no problem).

Re-execute the step 3 from the basic checks and verify that the NatNetwork is modified in the correct way. 

Modify the checkScript in order to add a "SSH Verify" NatRule, that shows if the Guest has the port 22 is redirected to the Host XX22 and 
display a message.

Make and screenshot and upload with the checkScript modified, and renamed as:

```shell
basicVBoxNATCheckSSH.sh
```

## Apache service test

Install apache2 (port 80) in the virtualmachine and create a NAT Portforwarding Rule to use the Host port **YY80**. 

If you are wondering about the host port, is easy to find it!, it is explained in the next paragraph:

```Interlude
Ayer, durante la charla de Autonomos, 
se dijo cual era la tasa de Autonomos 
que se tendría que pagar a partir de Enero.
Esa cifra, concatenada al puerto 80 
es el puerto del anfitrion al que se 
debe redirigir el tráfico del SSH de la máquina invitada.
```

Test the Apache from your Host machine using Firefox:

**http:\\\\YOUR_HOST_IP_HERE:YY80**

Re-execute the step 3 from the basic checks and verify that the NatNetwork is modified in the correct way.

Modify the checkScript in order to add a "Apache2 Verify" NatRule, that shows if the Guest has the port 80 is redirected to the Host YY80 and 
display a message.

Make and screenshot and upload with the checkScript modified, and renamed as:

```shell
basicVBoxNATCheckApache.sh
```

\newpage
## Transmission service test

Install transmission and configure it (from the GUI), to enable the WebServer and allow the remote control of the `.torrent` files and downloads.

![Transmission Remote](./imgs/transmission-ports.png)\

Set up the user : *smxsox* and password *smxsox*.

Modify your NAT PortForwarding table to allow the acces from the Host machine IP using the Host port **4291**. 

Access from your machine and check that everything is fine.

**http:\\\\YOUR_HOST_IP_HERE:4291**

Modify the checkScript in order to add a "Transmission Verify" NatRule, that shows if the Guest has the port 9091 is redirected to the Host 4280 and 
display a message.

Make and screenshot and upload with the checkScript modified, and renamed as:

```shell
basicVBoxNATCheckTorrent.sh
```
>>>>>>> a1aaf52a19e8d25b5f5aedd23985b8234d40b358
