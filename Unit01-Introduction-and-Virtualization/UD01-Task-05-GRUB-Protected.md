---
title: Unit 01 - TaskProject GRUB and Pass
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# TaskProject: GRUB and Password

In this initial task, we need to configure several Laptops (20 in total) to provide some kind of conectivity to the Classroom of Technology.

In this task, we will use a lot of skills (learning by doing), that are already previously learned by our students (unfortunately, before the summer xD). 


## Objectives

 - Configure all machines with dual boot, but we only permit GNU/LinuX OS by default.
 - If the user changes to Windows entry, this user must know the password for the Technology Department.
 - Both OS must be configured to use the WiFi of *LaSenia*.

## Steps

### 1: Virtualization

Create a VM with a hard disk with 75GB capacity, and then install windows 11 first and after that LliureX.

### 2: Create a Snapshot

When both OS will be installed, please make a *snapshot* to quickly restore the environment to a *valid situation*.

### 3: Configure GRUB

Follow the steps presented here:

 - [https://help.ubuntu.com/community/Grub2/Passwords](https://help.ubuntu.com/community/Grub2/Passwords)

and validate them (or not).

### 4: Notice the teacher

When everything has done...
