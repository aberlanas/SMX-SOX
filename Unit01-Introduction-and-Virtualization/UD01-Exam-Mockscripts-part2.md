---
title: Unit 02 - Exam of Mockscripts
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# Task

Using the file `MOCK_DATA_EXAM.csv` that you can found at Aules, code a shells scripts that makes the next checks programatically:


1. Show the `id` and the `first_name`, `ip_address` and `system_provider_frequency` from the lines that their `ip_address` is a Class B or C and the `system_provider_frequency` is Weekly or Monthly.
2. Show the `id` and the `movies` from the lines who have almos 2 genres of movies, and the `id` is not a prime number.
3. Show the `id`, `email` and `system_performance` from the lines which the `system_performance` is greater or equal to 42 and the email domain ends with `.com`, `.edu`, or `.gov`.
4. Show the `id`, `ip_address`, `ip_system_provider` and `system_performance` from the lines which this `id` are included in the both ip fields and their `system_performance` not or viceversa (**XOR**).
5. (Optional) Show the statistics (in  %) from each movie gender in the file.
