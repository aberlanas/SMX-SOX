---
title: Unit 01 - The Peloponessian Scripting War
subtitle: "Scripting in the Ancient Greece"
author: Angel Berlanas Vicente 
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-exam.pdf"
---

# Introduction

This is our first Challenge, or Exam, or test, or *whatever*. Please : **DON'T PANIC**. Your teacher's only purpose is testing your capabilities, your imagination and your skills when you face a *white-paper problem*.

Read all the questions before starting, and remember:

**DON'T PANIC!**

![Medical War](imgs/greeksvspersian.png)\

## Especial Thanks

This exam was inspired by the 300 Earth & Water Boardgame, designed by *Yasushi Nakaguro*, and one of my favorite opponents.

# Task 01: Medical Wars

In the Medical Wars, the Persian Empire fought versus the Alliance of Athenas and Sparta. From this war of the antiquity, we know a lot because the Greeks wrote a lot about this conflict.

One of the heroes of the Conflict was *Leonidas*, who fought with a reduced force versus the Persian Army in the Battle of Termopilas.

In this battle, Leonidas organized his army in several phalanxes. 

The phalanx is a very special fight formation that allowed the Greeks the use of their superior combat capabilities in front of the uncountable Persian Army.

The *phalanx* had only few posibilities:

 - 13 men formation, also know as a **Thebas phalanx**.
 - 17 men formation, also know as a **Delphi phalanx**.
 - 20 men formation, also know as a **Sparta phalanx**.

The choice of the number of men for the Battle depends only by the enemies they will face. The general choses the formation and says the word that indicates the soldiers to re-arrange in the correct *phalanx* formation.

The soldiers that could not be arranged in a *phalanx* were used as explorers.

Code a ShellScript that accepts 2 arguments:

- Number of soldiers of the greek army.
- Word that identifies the *phalanx* formation:
	- Thebas ( 13 men )
	- Delphi ( 17 men )
	- Sparta ( 20 men )
- If the explorers will be less than 5, then displays a message.


Examples of execution:

```shell
leonidas@esparta:~$./task-01.sh 300 Sparta
- 15 phalanx could be formed.
- Not enought explorers.
```

```shell
leonidas@esparta:~$./task-01.sh 149 Thebas
- 11 phalanx could be formed.
- 6 explorers.
```

![Task01](imgs/greeks01.png)\

\newpage
# Task 02 : Reading and Searching

Using the book of the **Peloponessian War** by *Thucydides*, that your teacher (amazing teacher) brings with the exam, as the *source of knowledge*, we must code a shellscript that allows the current reader extract info about the occurences of a given word inside the text.

The script must count the given word occurences inside the text (taking into account two or more occurences in the same line as different occurences), and displays a simple statistics. The search must be *case-sensitive*, and not include the word if it is inside "other":

For example, if you are looking for the word: **Ionia**

 - Ionia**ns** will not be matched.

As you know, the ancient Greeks categorised the importance of a word using these (the occurences), and you must do the same, following the next table:


| Number of Occurences of the given word | Name in Greek-Pantheon-measure-system|
|:--------------------------------------:|:------------------------------------:|
| 0                                      | Ourea |
| 1-12					 | Tartarus |
| 13-30				         | Hemera |
| 31-100				 | Nyx |
| 101-300				 | Chronos |
| 300+					 | Chaos |  

Examples of execution:

```shell
leonidas@esparta:~$./task-02.sh Ionia
- 21 Occurences 
- Level: Hemera
```

```shell
leonidas@esparta:~$./task-02.sh Athens
- 327 Occurences 
- Level: Chaos
```





![Task02](imgs/greeks01.png)\

 
