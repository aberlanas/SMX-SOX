---
title: Unit 02 - VirtualBox && Nat 
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# Task

Using ShellScript, PowerShell or Python create several Scripts in order to keep the mental sanity when you are working with VirtualBox machines and 
diferent configurations.

Checks:

1. List all the VBoxMachines configured.
2. For each of them, list the status and the network conectivity.
3. For each NatNetwork list the NatTable with port forwarding.
4. Additional checks (see in classroom).


## Python

[ VirtualBox Api ](https://www.virtualbox.org/sdkref/)

[StackOverFlow - Arreglando la versión](https://stackoverflow.com/questions/44477318/is-it-possible-to-use-the-python3-bindings-for-virtualbox)


## 

