---
title: Unit 01 - Mockscripts
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# Task

Using the file `MOCK_DATA.csv` that you can found at Aules, code a shells scripts that makes the next checks programatically:


1. Show the `username` from the lines that their `id` is a prime number. 
2. Show the `first_name` and `email` from the lines which `file_name` has the `.txt` extension.
3. Show the `first_name` and `username` from the lines which their `domain_name` has at least one subdomain.
4. Show the `first_name` and `plant_name` from the lines which their `plant_name` is composed *exactly* by 3 words.
5. Show the `username` and `ip_address` from the lines which their `ip_address` has a class A or B.
6. Show the `username` and `file_name` from the lines which their `file_name` has the `.gif` or `.GIF`.
7. Show the `first_name` and `email` from the lines which their email has the `.org` domain only in the `email`.
8. Show the `first_name` from the lines which their `first_name` starts with *d* and their `id`  is 7-multiple.
9. Show the `id`, `username`, `plant_name` which id is an odd number , 6 is a natural divisor and their `username` begins with `f`.
10. Show the `username` and `ip` from the lines which `ip` has the same last 8 bits thats the actual ip of the scripts running machine.
11. Show the `id` and `ip` from the lines which their `ip` in binary format has a prime number of ones.  
