---
title: \[SMB\] - SMB - I 
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# SMB y Sistemas de ficheros Remotos

SMB (*Server Message Block*) is one of the most famous *shared-folders* systems.
Could be the most used system, because it works well in mixed network environments.

In GNU/LinuX systems the implementation is also know as *samba*.

This system has a lot in common with NFS, but their configuration files are quite
different.

In order to achieve the correct performance of this (and next practices), you must 
understand *very well* from the very beginning:

- What am I doing?
- We know the *Client-Server* architecture?

Good luck! And thanks for all the fish!

# Windows Server

## Step 00 - Install a Windows Server

If you are Jose Losa, you can skip this step.

Configure it with a Admnistrator user and use the RedNat to connect this machine
with the others.

## Step 01 - SMB Server Service in Windows Server

This service is already installed.

## Step 02 - SMB Server Service Configuration

Now we are going to configure the next folders to share them across our NatNetwork.

We must create the next folder:

`C:\Compartido-Win-SMB\Samba-Di-Jane\`

![](./imgs/smb-nfs-12-07-18.png)

We must export this folder for the machines in our network. For now the User and the Password are
our Administrator Credentials.

In next episodes, we must configure all of these with *Active Directory*.

To do this we will follow the steps of the previous wizard, but
select *SMB (Fast)*.

In the share configuration parameters window
We will select the options shown in the following image:

![](./imgs/smb-nfs-11-42-01.png)

Once we have the folder being served by our Windows server, is the turn for client settings.

## Step 03 - SMB Client on Ubuntu

The first of all will be installing the support utilities on the client
**CIFS** , to allow the system mount operations
of remote files that are being served by our server.

`apt install cifs-utils`

Once we have the utilities installed, we are going to write a credentials file
that the mount client system will use to *authenticate us* on the server when it starts.

A good place to set it is in the user's home folder.
that we want you to mount. In this case our user is `root` and `root` is somewhat
special, we will set the file in `/root/.credentials`.

The content must be the data necessary to log into the
remote machine to mount the Unit (in my case the user
Administrator).

`$ cat /root/.credentials`

```shell
username=Administrador
password=Win4dm1n
domain=WORKGROUP
```

We need the next permissions: 0600.

`# chmod 0600 /root/.credentials`

\newpage
We use the `/etc/fstab` file to mount the shared folder in the boot process.
Take into account that the file is *splited in several lines*, but you must configure it 
**correctly**.

```shell
# <file system>
//192.168.1.59/Compartido-Win-SMB    

# <mount point>
/client/compartido-win-smb    

# <type>
cifs   

# <option>
credentials=/root/.credentials    

# <dump> <pass>
0   0
```

\newpage

![](./imgs/Gremlin_Leader.png)

The creator of the guide (*evil laugh*) has deliberately omitted something in
the configuration of the **client** that will not allow you to perform the
practice as if it were a tutorial.

Once you have set the configuration in the `/etc/fstab` file,
you should run the command `sudo mount -a` and investigate what happens
until it works.

Here much of what has been learned will be put into play. ^_^.
Once the assembly has been carried out correctly, if we now execute a
`tree -f /client/` will output something similar to this:

![](./imgs/smb-nfs-12-07-48.png)

Don't forget to comment the `fstab` line when you finish the practice and 
when I will check it, so that there are no problems later.


\newpage

# Tasks

1. Follow the steps and when you have it working, let me know.

2. Set up an additional folder on the SMB server:
C:\\Compartido-por-Goblins\\ and create a user "goblin" that
can read and write to it. Create another mount point on the
client:


`/client/dancing-goblins/`

And set it to be the user `goblin` at startup that does it.
mount (just like before \"Administrator\"). when you have it
working, let me know.

![](./imgs/Mad-gremlin.png)
