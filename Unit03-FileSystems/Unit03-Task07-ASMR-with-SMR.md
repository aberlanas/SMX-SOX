---
title: ASMR with SMR
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# ASMR with SMR

Make a VideoTutorial using ASMR about the technology that is asigned with the raffle:

| Student | Technology |
|---------|------------|
| Ramon   | DHCP       |
| Sergio  | Shell Script Arguments |
| MarcOS  | rsync |
| Samuel  | Encryption |
| JoseLosa| Python3    |
| Bruno   | Markdown   |
|Jesus    | Conditionals|
| JorgeE  | git |
| Danny   | SSH |
| Marc    | Samba |
| Pau     | SCRUM |
| Puertas | Matemáticas | 
| Erik    | Apache |
| Raul    | PowerShell |
| Daniel  | Terminal |
| Diego   | Port Forwarding |
| A.Pardo | NFS  |
| Jorge C. | DNS  |
| A.Escartí | Permisos |
| Alex    | Netplan |

The video must be between  10-15 minutes. The chosen language must be *ENGLISH*.

# Useful links:

- [ASMR Info](https://en.wikipedia.org/wiki/ASMR)
- [OBS](https://obsproject.com/es)