---
title: A Simple Friday Exam with NFS
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents
\newpage
#  A Simple Friday Exam with NFS

Please read all the questions before starting.

![Seafarers](imgs/seafarers01.png)\

*Warning to Seafarers*

`PATHS`, `URLS` and common stuff for scripts could be placed as the first lines of the scripts, in order to stablish at the beginning the 
values and be ready to be changed if the **sysadmin** needs.

## Task 01 : Environment and Paths 

In our enterprise, our DevOps CEO  has several hobbies, one of them are the Boardgames (as you know), and Solitaries. One of the most famous solitaries 
from all times is **FRIDAY**, a very interesting game which *core-mechanic* is *DeckBuilding*.

\begingroup
\centering
![Friday Cover](./imgs/friday-cover.png){width="300px" height="300px"}\
\endgroup

\newpage
### Steps
In this game we have several fases or steps:

1. Green Level.
2. Yellow Level.
3. Red Level.
4. Pirates.

For indicating the level, the game has several cards which colours are useful to allow the user remembering which is the level of danger from the perils cards.

\begingroup
\centering
![Friday Perils](./imgs/friday-perils.png){width="200px" height="400px"}\
\endgroup

This cards are useful, but our DevOps CEO wants several folders to keep his mind clear and ready to fight with the puzzles that the game will present play after play.

\newpage
### Script 
We need a Script, written in PowerShell, Python or BASH that:

- Accepts only one parameter, if more are given it must display an error message.
- The parameters must be **case-sensitive**.
- If the parameter is `clean`, then the Script must delete (if exists) the previous "games-folders" in our **CURRENT USER HOME DIRECTORY**.
- If the parameter is `start`, then the Script must create a folder with name `Friday-for-Friday` in our **CURRENT USER HOME DIRECTORY**.
  and create inside a file that contains the time and date of the Start of the Game (for statistics purposes), named : `time-friday-start.tkn`
- If the parameter is `green`, then the Script must create a folder inside the `Friday-for-Friday` folder with the name "Green-Level", and create inside 
   a file that contains the time and date of the Start of the GreenLevel (for statistics purposes), named : `time-friday-green.tkn`.
- If the parameter is `yellow`, then the Script must create a folder inside the `Friday-for-Friday` folder with the name "Yellow-Level", and create inside 
   a file that contains the time and date of the Start of the YellowLevel (for statistics purposes), named : `time-friday-yellow.tkn`.
- If the parameter is `red`, then the Script must create a folder inside the `Friday-for-Friday` folder with the name "Red-Level", and create inside 
   a file that contains the time and date of the Start of the RedLevel (for statistics purposes), named : `time-friday-red.tkn`.
- If the parameter is `pirates`, then the Script must create a folder inside the `Friday-for-Friday` folder with the name "PIRATES", and create inside 
   a file that contains the time and date of the Start of the PIRATES Level (for statistics purposes), named : `time-friday-pirates.tkn`.
- If the parameter is `end`, then the Script must create inside the `Friday-for-Friday` folder and a file inside that contains the time and date 
of the End of the GAME (for statistics purposes), named : `time-friday-end.tkn`.

\newpage
### Level UP Restrictions

If the user try to **skip** some level-up step, the script must display an error message and do nothing.
If the user uses the `end` parameter, the game is over (you can lose in all levels), and you cannot add new levels to the game.

### Statitics (BONUS ACTION)

If the parameter is "statistics" (case-sensitive), then the script must display a simple statistics in minutes/seconds for each Level.

\begingroup
\centering
![Friday](./imgs/friday-desconcentrado.png){width="200px" height="400px"}\
\endgroup

\newpage
## Task 02: NFS

In one of our Xubuntu Machines or Ubuntu Server (from now our **Server System**):

- Install the `nfs-kernel-server` service using apt.
- Create 4 folders in the next PATHS:
   - `/srv/friday/level-green`
   - `/srv/friday/level-yellow`
   - `/srv/friday/level-red`
   - `/srv/friday/pirates`
- Copy in `/srv/friday/level-green` the current `sources.list` of the Server System.
- Copy in `/srv/friday/level-yellow` the file `/etc/hosts` of the Server System.
- Copy in `/srv/friday/level-red` the file `/etc/fstab` of the Server System.
- Configure the NFS Service in order to serve the three coloured levels to all the machines in *read only*.
- Configure the NFS Service in order to serve the `pirates` folder only for you other Xubuntu Machine (IP) in *read write*.
- Copy in `/srv/friday/pirates` the file `/etc/exports` of the Server System.
- Install the `nfs-common` package using apt on client.
- Create a shell script in the Client Machine that creates the next mount points (if not exists):
   - `/mnt/bgg/friday/level-green`
   - `/mnt/bgg/friday/level-yellow`
   - `/mnt/bgg/friday/level-red`
   - `/mnt/bgg/friday/pirates`
   - And mount the directories served in their respectives folder (the same name)
- Execute the `mount` command without arguments in order to see that everything is Ok.

### Upload to Aules:

#### From the Server

- The file `/etc/exports`
- An Screenshot of the command output: `tree -f /srv/friday`

#### From the Client

- The Script.
- An Screenshot of the command output: `mount | grep friday` 


