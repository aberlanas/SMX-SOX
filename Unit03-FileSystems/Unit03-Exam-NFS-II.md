---
title: A Terminated! Exam
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents
\newpage

#  A Simple Friedemann Friese Exam with NFS

## Introduction

Please read all the questions before starting.

![Seafarers](imgs/seafarers01.png)\

*Warning to Seafarers*

`PATHS`, `URLS` and common stuff for scripts could be placed as the first lines of the scripts, in order to stablish at the beginning the 
values and be ready to be changed if the **sysadmin** needs.

## Finished! Lore

In our enterprise, our DevOps CEO  has several hobbies, one of them are the Boardgames (as you know), and Solitaries. One of the most famous solitaries 
from all times is **Terminated!**, a very interesting game from the author of Friday (the last exam) that is *Friedemann Friese* which *core-mechanic* is *SortingCards*..

Finished! is a puzzle-solving game, a completely new way to play solitaire!

It is a typical day at work. Your working schedule is chaotic as always and it‘s time to focus on the task at hand. 
Start sorting files and do not fall asleep. If you require a jolt of caffeine or rush of sugar, 
there is a limited supply of coffee and a small stash of sweets to help complete your tasks and get finished!

The cards are sorted from the 1 to the 48, in this Exam these cards are represented by several folders inside the *Finished* folder served using NFS from the ClassRoom Server. 

\begingroup
\centering
![Finished Cover](./imgs/finished.png){width="300px" height="300px"}\
\endgroup
\newpage

## Script

We need a Script that must be executed each 5 seconds, that must order the folders that are served via NFS from the Server of ClassRoom 4 in ascending order.
Indicating for each *gap* generated which is its size.

The Script must:

* Test if you have *root* permissions when executing the script.
* Display the current **Hour and Minute** (only).
* Test if the mount point in the client is present in `/mnt/finished`. If not exist must be created.
* Mount the nfs folder from the server in `/srv/nfs/finished-on-server`.
* List all the folders inside the previous folder and sort them in ascending order.
* For each *gap* present in the sorted list, the script must display the *gap-size*.
* If not gap exists, the script must finish and display a message of Success.
* The script must use a loop structure to execute all the steps each **5** seconds.

Example of execution:

```shell
* You Have Root Permissions
* Time: 12:31
* The mount point exists at /mnt/finished/
* The cards present are:
Card-01
Card-13
Card-14
Card-48
* 2 Gaps Existing
1 Gap: 12 elements remains.
2 Gap: 34 elements remains.
```



