---
title: \[SMB\] - Heart of Darkness from SAMBA
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# Samba and Heart of Darkness

Samba is an ancient dance from African continent. In this exam we want to make a simple
tribute to the African continent exploration.

\begingroup
\centering
![Heart of Darkness](./imgs/heartOfDarkness.png)\
\endgroup

\newpage
## Task 01

Create a Shared folders from the Windows Server 
(if you are Jose Losa, you can use the Ubuntu Server), 
using Samba with the next PATHs:

```shell
C:\Africa\Nile\
C:\Africa\Congo\
C:\Africa\Niger\
```

Every folder must be accessed only in *Read-Only* mode. You must create some example files inside the folders
in order to test when everything will be correct.

## Task 02

In the Xubuntu Client, prepare a Shell Script that mount each folder served by Windows in different PATH.
These PATHS must be foreach river, one actual country that has the river inside its borders.

For example:

```shell
C:\Africa\Nile ---> mounted in ---> /mnt/samba/egypt
```

**WARNING** Egypt can not be used ^_^.

The script must for each River:

* Test that you are executing it with root permissions.
* Create the mount point if not exists.
* Create the credentials file if not exists and set the correct contents.
* Check the correct permissions, owner and group for the credential file. (0600,root,root).
* Mount the folder 
* If everything is correct must display the line that could be put into the `/etc/fstab` file 
  (but not write on it),only display the line in the terminal.

For each test or check: display a message with information about the process. 
If something was wrong, the script must diplay an error message and not
continue **with the steps needed by this river**, but must continue with the next (if any) river remaining.

# Aules

- Upload the script to Aules.
- Upload a successful execution screenshot to Aules.
- Upload a screenshot from the shared folders in Windows Server.

Good luck to everyone