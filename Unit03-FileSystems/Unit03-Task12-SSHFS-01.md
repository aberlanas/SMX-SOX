---
title: \[SMB\] - SSHFS and Company
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# SSH, rsync, find

Create in your GitLab repository of SOX a folder named : `utils`, inside of them
create a Markdown with information about:

* ssh
* rsync
* find

