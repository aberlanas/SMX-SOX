---
title: \[NFS\] - NFS y RAIDs
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# NFS y Sistemas de ficheros Remotos

## RAID 0

 - Añade dos discos de 10 Gigas a la máquina Virtual con Xubuntu para SOM.
 - Durante la creación de ambos, reserva el espacio *fijo* de los discos.
 - Arranca Xubuntu y detecta qué discos acabamos de conectar.
  - ¿Qué comandos podemos usar?
 - Instala `mdadm`.
 - Configura un dispositivo lógico en `/dev/md0` que sea un RAID 0 con los 
   discos que acabas de añadir.
 - Formatea el nuevo dispositivo lógico con el sistema de ficheros `ext4`.
 - Montalo en `/srv/nfs/raidzero/`
 - Añadelo a `/etc/fstab`
 - Configura el servicio *NFS* para que lo exporte a toda la red en sólo lectura.
 - Móntalo en otra máquina y comprueba que el espacio compartido es la *SUMA* de ambos discos.


## RAID 1

 - Añade dos discos de 10 Gigas a la máquina Virtual con Xubuntu para SOM.
 - Durante la creación de ambos, reserva el espacio *fijo* de los discos.
 - Arranca Xubuntu y detecta qué discos acabamos de conectar.
  - ¿Qué comandos podemos usar?
 - Instala `mdadm`.
 - Configura un dispositivo lógico en `/dev/md1` que sea un RAID 1 con los 
   discos que acabas de añadir.
 - Formatea el nuevo dispositivo lógico con el sistema de ficheros `ext4`.
 - Montalo en `/srv/nfs/raiduno/`
 - Añadelo a `/etc/fstab`
 - Configura el servicio *NFS* para que lo exporte a toda la red en lectura-escritura.
 - Móntalo en otra máquina y comprueba que el espacio compartido es del tamaño de uno de los discos.