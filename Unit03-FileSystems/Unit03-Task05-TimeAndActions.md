---
title: Useful Actions for Users at Solar System
subtitle: "Unit 03"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Introduction

![farers](imgs/seafarers01.png)\

*Warning to Seafarers*

`PATHS`, `URLS` and common stuff for scripts could be placed as the first lines of the scripts, in order to stablish at the beginning the 
values and be ready to be changed if the **sysadmin** needs.

The examples for this task are presented in a Windows OS, but you can imagine the correct paths for GNU/LinuX.

# Task 01

Choose one language: BASH, PowerShell or Python, and then code a Script that:

- Accepts 3 argument.
- The first one must be a **valid user** of the System. In other case, the script must exit and display a message:

```PowerShell
 * Error - The User given not exists.
```

- The second argument must be one of these words:
    - `replenish`
    - `test`
    - `clean`

The behaviours of the actions are described below:

\newpage
## replenish

In the `HOME` directory of the Given user, the script must create the next folder structure:

```Shell
 -- SolarSystem/
           Mercury/
           Venus/
           Mars/
           Jupiter/
           Saturn/
           Uranus/
           Neptune/
```

In each of the folders of the Solar System Planets the script must create a single text file with the name of the user as the content of the file with the date of the creation. The file must be named as: `control-planet.txt`.


```Shell
 -- SolarSystem/
           Mercury/
				control-planet.txt
           Venus/
				control-planet.txt
           Mars/
				control-planet.txt
           Jupiter/
				control-planet.txt
           Saturn/
				??????
           Uranus/
				control-planet.txt
           Neptune/
				control-planet.txt
```

In the Saturn folder (because has rings), you must NOT include this file and instead of it, you must include a file named as :

```
Environment + "the user given " + "the date in format YYYYmmdd-hhMM " + ".txt"
```

Wich content will be the list of the current Environment Values for this user. 

```Shell
user@mars:~$./solarSystem.sh admin replenish "All is done"
# Doing stuff
# Creating file at:
/home/admin/SolarSystem/Saturn/Environment-admin-20221212-1030.txt
.....
.....
All is done
```


In the `replenish` action, the third argument must be Written as a message when everything was complet, as a "All is done" message.


## test

If the second argument is test, then the script must accepts a third argument that must be a valid PATH to a SolarSystem folder. The script must test if the folder is in a *correct place* and test if the user given has Read Permissions of all the files and folders under the PATH given.

Examples of usage:


```Shell
user@mars:~$./solarSystem.sh admin test /non-solarSystem-directory
 * Test 1 : The user admin exists.
 * Test 2 : The action test is a valid action.
 * Error! - The folder given to test not exists: /non-solarSystem-directory
```

Now, if the previous behaviour (with replenish) was run for the user *Administrador*, the output must be:


```Shell
user@mars:~$./solarSystem.sh admin test $HOME/SolarSystem
 * Test 1 : The user admin exists.
 * Test 2 : The action test is a valid action.
 * Test 3 : The folder given to test exists: /home/admin/SolarSystem
 -- The folder : /home/admin/SolarSystem/Venus exists and could be read by admin.
 -- The folder : /home/admin/SolarSystem/Mars exists and could be read by admin.
 ...

```

## clean

If the second argument is clean, then the script must accepts a third argument that must be a valid PATH to a SolarSystem folder. The script must test if the folder is in a *correct place* and remove it and its files and sub-folders.

