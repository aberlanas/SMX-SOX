---
title: \[SOX\] 2023 - Recover Exam (Reloaded)
subtitle: "Time: 08:00-09:50 (1:50 hours)"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
toc: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Recover Exam - 2023 (Reloaded)

\tableofcontents

## Introduction

In this exam you will use the infraestructure as being demanded during the course. Only 1 Xubuntu Client and 1 Ubuntu Server without X.

Read *carefully* all the exam *before* starting the tasks.

The exam presentation (a *.zip* file), must contain several folders (one for each Task), and several subfolders (for each Task). For example, the Task01 folder must be as follows:

```shell
usuario@maquina~:$ tree -f recover-entrega/Task01
```

![Task01-Entrega](imgs/entrega-tree.png)\


**If you use AI, I will know, and all the exam will be failed**.

\newpage

## Task 01 - NFS (3 points)

### Server Part (1 point)

Configure the *NFS* in the *ubuntuserver.smx2023.net* in order to provide 3 remote mountpoints:

The mountpoints must serve the next folders:

 * `/srv/matrix/city-of-machines/`
 * `/srv/matrix/oracle/`
 * `/srv/real-world/Sion/`
 

Please put into them, some *dummyfiles* to recreate a more *realistic* environment. The use of the `touch` command could be a good idea.

This folders (or mountpoints) must be configured in the next way:

| Mount point | Clients allowed | Operations |
|-------------|-----------------|------------|
| `city-of-machines`| All internal LAN | Read Only  |
| `oracle`      | IP Xubuntu Client | Read and Write |
| `Sion` 	| IP Xubuntu Client | Read Only |

Inside the `/srv/real-world/Sion/`, generate two folders :

* `morpheo`
* `trinity`

And in each one, copy the PDF of the Exam with the name of the folder, as follows:

* `/srv/real-world/Sion/morpheo/morpheo_Exam.pdf`
* `/srv/real-world/Sion/trinity/trinity_Exam.pdf`

Inside the `/srv/matrix/city-of-machines` copy the png of the Architect that you can find with the exam.
Inside the `/srv/matrix/oracle` copy the png of the Oracle that you can find with the exam.

This will be useful for the last part.

![Morpheo](imgs/morpheo.png)\

----

**Files to upload**

- nfs-kernel-server configuration file.
- screenshot of the tree -f command over the `/srv/matrix/`.
- screenshot of the tree -f command over the `/srv/real-world/`.
- screenshot of the `showmount -e localhost` command output.
- screenshot of the `showmount -e ubuntuserver.smx2023.net` command output.

----

\newpage

### Clients Part (2 points) 

#### GNU/LinuX

Configure the Xubuntu Client to mount :

 - `city-of-machines`
 - `oracle`
 - `Sion`

On the next folders:

* `city-of-machines` $\rightarrow$ `/connection/ro/city-of-machines`
* `oracle`  $\rightarrow$ `/connection/oracle-house`
* `Sion` $\rightarrow$ `/connection/secure/last-human-city`

This must be performed on the boot process of the machine, using the *fstab* file. 

To prevent a critical system failure, we must **create a ShellScript** that makes the next actions:

* All the actions of the script must show a progress message.
* Check if the user that launchs the script has root privileges.
* Check if the client folders exists (each one), and if not create them.
* Check if the servermachine is *available* using `ping` with 3 checks.
* Check if the mountpoints are already mounted and umount them in this case.
* Using the `mount -a` command, let the system ready to be used.

![Trinity](imgs/trinity.png)\

----

\newpage
**Files to upload**

- Screenshot of the mounted directories (using `tree`).
- The ShellScript.
- The fstab file.

----

\newpage
## Task 02 - OpenLDAP (7 points)

### Server Part (1 points)

Configure the *openldap* server in the *ubuntuserver.smx2023.net* to be access from a web-browser by the client at:

 - http://ubuntuserver.smx2023.net/phpldapadmin

You can use the guide from the Moodle Course (SMX-SOX). But remember that the exam assumes that:

- You can *resolve by name* the server.
- You has correctly installed the slapd daemon, the apache and the phpldapadmin.
- **Some** of the changes must be:

For Exam:

| Key | Recover Exam |
|-----|--------------|
|ldap_search_base |dc=ubuntuserver,dc=smx2023,dc=net|
|ldap_uri| dc=ubuntuserver,dc=smx2023,dc=net|
|ldap_default_bind_dn| cn=admin,dc=ubuntuserver,dc=smx2023,dc=net| 
|ldap_user_search_base| cn=matrix,dc=ubuntuserver,dc=smx2023,dc=net |

These are only few examples, you must configure *correctly* the LDAP and the PHPLdapAdmin.

Create a new cn:

- `cn=matrix,dc=ubuntuserver,dc=smx2023,dc=net`

And 2 users inside this cn:

- morpheo
- trinity

The user `morpheo` must have the ID: 7777.
The user `trinity` must have the ID: 8888.

And configure the `$HOME` directory in the path:

- `/home/Nebuchadnezzar/$USER`

![Oracle](imgs/oraculo.png)\

----

**Files to upload**

- the sssd configuration file: `/etc/sssd/sssd.conf`
- screenshot of the webbrowser listing the users.
- screenshot of `getent passwd` output in the clients 

----

\newpage

### Clients Part (6 points)

Configure the *ldap client* in the *ubuntuclient.smx2023.net* to allow the users recently created on the server make a Graphical Login using lightdm:

Create a Shell Script that must be executed on the graphical login of the user, that must be perform the next steps:

1. **log** in the `syslog` of the Client System the user that is currently logining at the machine. 
2. All the operations in the script must be logged in the StandarOutput and the `syslog` (using `logger`).
3. Check if the user is local or it is from LDAP.
4. If the user is local, the script must create a *symbolic link* to `/connection/ro/city-of-machines`` in the `Desktop` folder of the user with the name of the user : `$USER-connected`. If the user has a previous link to this folder, the script must remove it first. (*logging all operations*).
5. If the user is from LDAP and the is inside the `cn=matrix,...`, the script must create two *symbolic links* at his (or her) Desktop folder, one to the PDF of the User (obviously to the correct name) directly, and the other to the folder `/connection/oracle-house` with the name "ORACLE" (yes, in uppercase).
6. If the user is from LDAP and has a **odd** uid, the script must create a copy of the PDF (following the link) in the `$HOME` folder with the date of today in this format: `YYYYMMDD-hhmm-Exam.pdf` : Example : *20230301-0855-Exam.pdf*.
7. If the user is from LDAP and has an **even** uid, the script must create a tar of the folder `/connection/oracle-house` with the date of today in this format: `YYYYMMDD-hhmm-ORACLE-DATA.tar.gz` : Example : *20230301-0855-ORACLE-DATA.tar.gz*.

![Architect](imgs/arquitecto.png)\

----

**Files to upload**

- Two Screenshots (one for each user) of a successful graphical session.
- Two Screenshots (one for each user) that shows the output from the execution of : `pwd` command in a `xfce4-terminal` .
- Screenshots (one for each LDAPuser and one for local user), of the the Desktop folder of the users showing the content (*symbolic links*), and the files created.
- The Script itself.
- The Script Autostart configuration.
- The `tar.gz` file created with the script in the step 7.

----

