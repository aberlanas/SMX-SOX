---
title: \[SOX\] 2023 - Recover Exam
subtitle: "Time: 12:00-13:30 (1:30 hours)"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
toc: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Recover Exam - 2023

\tableofcontents

## Introduction

In this exam you will use the infraestructure as being demanded during the course. Only 1 Xubuntu Client and 1 Ubuntu Server without X.

Read *carefully* all the exam *before* starting the tasks.

The exam presentation (a *.zip* file), must contain several folders (one for each Task), and several subfolders (for each Task). For example, the Task01 folder must be as follows:

```shell
usuario@maquina~:$ tree -f recover-entrega/Task01
```

![Task01-Entrega](imgs/entrega-tree.png)\

\newpage

## Task 01 - NFS (3 points)

### Server Part (1 point)

Configure the *NFS* in the *ubuntuserver.smx2023.net* in order to provide 3 remote mountpoints:

The mountpoints must serve the next folders:

 * `/srv/extra/general-pool`
 * `/srv/extra/exclusive-pool-by-ip`
 * `/srv/extra/net-home/users/`
 

Please put into them, some *dummyfiles* to recreate a more *realistic* environment. The use of the `touch` command could be a good idea.

This folders (or mountpoints) must be configured in the next way:

| Mount point | Clients allowed | Operations |
|-------------|-----------------|------------|
| `general-pool`| All internal LAN | Read Only  |
| `exclusive-pool-by-id` | IP Xubuntu Cliente | Read and Write |
| `users` 	| IP Xubuntu Cliente | Read Only |

Inside the `/srv/extra/net-home/users/`, generate two folders :

* `koboldWarrior`
* `koboldBoss`

And in each one, copy the PDF of the Exam with the name of the folder, as follows:

* `/srv/extra/net-home/users/koboldWarrior/koboldWarrior_Exam.pdf`
* `/srv/extra/net-home/users/koboldBoss/koboldBoss_Exam.pdf`

This will be useful for the last part.

----

\newpage
**Files to upload**

- nfs-kernel-server configuration file.
- screenshot of the tree -f command over the `/srv/extra/`.
- screenshot of the `showmount -e localhost` command output.
- screenshot of the `showmount -e ubuntuserver.smx2023.net` command output.

----

\newpage

### Clients Part (2 points) 

#### GNU/LinuX

Configure the Xubuntu Client to mount :

 - general-pool
 - exclusive-pool-by-ip

On the next folders:

* `general-pool` $\rightarrow$ `/client/ro/general-pool`
* `exclusive-pool-by-ip`  $\rightarrow$ `/client/rw/exclusive-pool-by-ip`
* `users` $\rightarrow$ `/client/ro/homes`

This must be performed on the boot process of the machine, using the *fstab* file. 

To prevent a critical system failure, create a ShellScript that makes the next actions:

* All the actions of the script must show a progress message.
* Check if the user that launchs the script has root privileges.
* Check if the client folders exists, and if not create them.
* Check if the servermachine is *available* using `ping` with 3 checks.
* Check if the mountpoints are already mounted and umount them.
* Using the `mount -a` command, let the system ready to be used.

----

**Files to upload**

- Screenshot of the mounted directories.
- The shellScript.
- The fstab file.

----

\newpage
## Task 02 - OpenLDAP (7 points)

### Server Part (2 points)

Configure the *openldap* server in the *ubuntuserver.smx2023.net* to be access from a web-browser by the client at:

 - http://ubuntuserver.smx2023.net/phpldapadmin

You can use the guide from the Moodle Course (SMX-SOX). But remember that the exam assumes that:

- You can *resolve by name* the server.
- You has correctly installed the slapd daemon, the apache and the phpldapadmin.
- **Some** of the changes must be:

Original

|  Key | Original from this course |
|------|---------------------------|
| ldap_search_base | dc=ubuntusrv,dc=smx2023,dc=net|
| ldap_uri |dc=ubuntusrv,dc=smx2023,dc=net|
| ldap_default_bind_dn|  cn=admin,dc=ubuntusrv,dc=smx2023,dc=net | 
| ldap_user_search_base|  cn=kobolds,dc=ubuntusrv,dc=smx2023,dc=net| 

Recover Exam

| Key | Recover Exam |
|-----|--------------|
|ldap_search_base |dc=ubuntuserver,dc=smx2023,dc=net|
|ldap_uri| dc=ubuntuserver,dc=smx2023,dc=net|
|ldap_default_bind_dn| cn=admin,dc=ubuntuserver,dc=smx2023,dc=net| 
|ldap_user_search_base| cn=kobolds,dc=ubuntuserver,dc=smx2023,dc=net |

These are only few examples, you must configure *correctly* the LDAP and the PHPLdapAdmin.

Create a new cn:

- `cn=kobolds,dc=ubuntuserver,dc=smx2023,dc=net`

And 2 users inside this cn:

- koboldWarrior
- koboldBoss

And configure the `$HOME` directory in the path:

- `/home/kobolds/$USER`
  
----

**Files to upload**

- the sssd configuration file: `/etc/sssd/sssd.conf`
- screenshot of the webbrowser listing the users. 

----

\newpage

### Clients Part (5 points)

Configure the *ldap client* in the *ubuntuclient.smx2023.net* to allow the users recently created on the server make a Graphical Login using lightdm:

Create a Shell Script that must be executed on the graphical login of the user, that must be perform the next steps:

1. **log** in the `syslog` of the Client System the user that is currently login at the machine. 
2. All the operations in the script must be logged in the StandarOutput and the `syslog`.
3. Check if the user is local or it is from LDAP.
4. If the user is local, the script must create a *symbolic link* to `/client/ro/general-pool` in the `Desktop` folder of the user. If the user has a previous link to this folder, the script must remove it first. (*logging all operations*).
5. If the user is from LDAP and the is inside the `cn=kobolds,...`, the script must create two *symbolic links*, one to the PDF of the User (obviously to the correct name), and the other to the folder `/client/rw/exclusive-pool-by-ip` with the name `EXCLUSIVE-FOLDER`.


----

**Files to upload**

- Two Screenshots (one for each user) of a successful graphical session.
- Two Screenshots (one for each user) that shows the output from the execution of : `pwd` command in a `xfce4-terminal` .
- Three Screenshots (one for each LDAPuser and one for local user), of the the Desktop folder of the users showing the content (*symbolic links*).
- The Script.
- The Script Autostart configuration.

----

