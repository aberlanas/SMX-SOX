---
title: \[Python\] Python y Hojas de Cálculo
subtitle: "Conociendo XlsxWriter"
author: Patxi y Angel
header-includes: |
lang: es-ES
keywords: [Python, Maths]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia-task.pdf"
---

# Python y Excel

Ya sabéis que Python es un lenguaje de propósito general que tiene un montón de plugins, librerías, etc. que amplian de manera sencilla su funcionalidad y permiten a sus desarrolladores la creación de herramientas y utilidades muy potentes utilizando pocas líneas de código.

Uno de los tipos de fichero que más se utilizan dentro de las empresas y como intercambio de información, son los ficheros `csv` (*Comma Separated Values*), que evolucionaron hacia los ficheros de hojas de cálculo hace algunos años en los ecosistemas hacen necesarias las fórmulas, el formato, etc.

Además, dentro del **BIG DATA**, los ficheros de Hojas de Cálculo permiten presentar datos de manera ordenada, filtrar, etc.

Como no podía ser de otra forma, python cuenta con *bindings* para la lectura y edición y creación de ficheros de hojas de cálculo (`.xlsx`).

Vamos a plantear un pequeño ejercicio para poner en práctica nuestras habilidades con este maravilloso lenguaje.

## Python XlsxWriter

La página oficial de la librería es:

* [https://xlsxwriter.readthedocs.io/](https://xlsxwriter.readthedocs.io/)

Podéis instalarla en Sistemas GNU/LinuX basadas en Debs utilizando `apt`

```shell
sudo apt install python3-xlsxwriter
```  

En caso de usar otros sistemas, seguid las instrucciones que se describen en la página oficial.


## Descripción del problema

Cuando se hace uso de los formularios de Google o de Microsoft que presentan una encuesta para recoger datos que han de rellenar los usuarios, el resultado suele presentarse en una hoja de cálculo con una fila por cada una de las respuestas.

![Excel - Recogiendo datos](imgs/excel-tic-01.png)\

Lo que hace el Coordinador TIC cuando lee las diferentes incidencias y las va resolviendo, es ir marcando con colores y añadiendo información     

![Excel - Resolviendo Incidencias](imgs/excel-tic-02.png)\

Lo que ocurre es que si al dia siguiente se vuelve a comprobar el listado de incidencias, se genera un nuevo excel que contiene las ya resueltas + las nuevas y se ha perdido el formato y los comentarios que se hayan podido hacer.

La tarea consiste en realizar un Script en Python, que usando la librería `xlsxwriter` lea el fichero generado y añada al fichero que contiene las modificaciones las filas que representan las nuevas incidencias. Respetando todo lo editado anteriormente.

Se adjuntan tres ficheros para que el alumnado tenga con *qué* trabajar.

- `NuevaIncidencia_20220921` -> Incidencias a fecha del dia 21.
- `NuevaIncidencia_20220923` -> Incidencias a fecha del dia 23. (contiene todas las incidencias hasta ese día).
- `NuevaIncidencia_20220921_RESOLVIENDO` -> Incidencias a fecha del dia 21, con formato y anotaciones.

La idea es que el programa, tomando con argumentos el fichero de incidencias del dia 23, añada al fichero de "RESOLVIENDO" las líneas que faltan (sin aplicar formato a las líneas nuevas).

\newpage
## Tarea

Realiza una clase en python que implemente los métodos necesarios para realizar la tarea descrita antes.

Se adjunta un pequeño esqueleto, que puede servir como base

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xlsxwriter
import os

def is_an_excel_file(filePath):

    is_an_excel = False

    try:
        workbook  = xlsxwriter.Workbook(filePath)
        is_an_excel = True
    except Exception as e:
        print(" Is not an Excel file")
    
    return is_an_excel

def is_a_file(filePath):

    if os.path.exists(filePath):
        return True
    else:
        return False


```



Si queda alguna duda, preguntadle a vuestro profesor.


