---
title: \[Python\] Excel y Python Merger
subtitle: " Descubriendo XlsxWriter "
author: Patxi y Angel
header-includes: |
lang: es-ES
keywords: [Python, Maths]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---


# Python y XLSX

Cómo ya sabéis, python es un lenguaje que dispone de multitud de herramientas y de *bindings* con librerias que nos permiten extender sus capacidades y facilitarnos la vida enormemente.

Vamos a realizar una actividad que manipule ficheros XLSX (hojas de cálculo).

## Librería

* [https://pypi.org/project/XlsxWriter/](https://pypi.org/project/XlsxWriter/)

## Instalación de la librería

En sistema Debian y Derivados (Ubuntu):

```shell
sudo apt install python3-xlsxwriter
```

En Windows, pues os lo buscáis y tal.

## Tarea 

Contamos con sistema de generación de *Excels* que nos genera un listado con una serie de respuestas a un formulario.

![Ejemplo](imgs/excel01.png)\


