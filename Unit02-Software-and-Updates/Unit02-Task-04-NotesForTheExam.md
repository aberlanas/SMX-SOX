---
title: \[Task Pre-test\] - Notes for the Exam
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Notes about

Take notes about the exam.

You will need PowerShell and BASH scripts skills. 

1. Port Forwarding and beyond.
2. Deal with arguments.
3. Loops for and while
4. Tests about files and folders.
5. Apps configuration.
6. Some basic math operation until neperian logarithms.
7. Software management (updates, installation, de-installation).
8. Working with files and patterns.
9. Network Operations.
10. Compression methods.

Upload a Markdown or PDF with the notes to Aules.