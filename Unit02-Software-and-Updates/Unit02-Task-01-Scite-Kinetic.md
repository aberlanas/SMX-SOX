---
title: Unit 02 - Scite from Kinetic
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# Software Managers and Installers - SciTE

In this practice we are going to see how to install a package from Kinetic

# SciTe Installation

Search in the Ubuntu Repository System, the **Kinetic** Version of SciTE and install it on your Xubuntu Virtualized.

Make a screenshot of the credits where you can see the version currently installed.

# Configure the SciTE

In the "Open File" dialog, show **All Files** instead **All Source** in the `Select Menu`.

