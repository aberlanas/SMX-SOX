---
title: \[EXAM Model B\] - Maximilian Graves
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# Introducion and GitLab

## Mandatory : Pre-Requisites

Prepare a document with your name (.odt,or .pdf), in the form:

- *Username_LastName_ExamModelB.odt*
- *Username_LastName_ExamModelB.pdf*

Create an account using a *professional* nickname on the GitLab system:

- [GitLab.com Sign-up](https://gitlab.com/users/sign_up)

When the process will be finished, make and screenshot, add it to the document 
and insert your new GitLab dashboard url in the document previously created:

For example, my gitlab webpage is:

- [https://gitlab.com/aberlanas](https://gitlab.com/aberlanas)

The time is limited.

![Seafarers](imgs/seafarers01.png)\

*Warning to Seafarers*

`PATHS`, `URLS` and common stuff for scripts could be placed as the first lines of the scripts, in order to stablish at the beginning the 
values and be ready to be changed if the **sysadmin** needs.

Read all the exam to the end **before** start to doing anything.

\newpage
# Task 01: Preparing the environment (PowerShell)

In the Arkham Old Town, near the **Northen Church** we can find an old man that needs our help. He is the **Official Graveyard Gatekeeper** of Arkham.

He needs an Scrip in PowerShell that provides the environment for his daily work at the Graveyard.

Every day, Maximilian Graves (*aka* Max), create a folder structure inside his $HOME (**or equivalent in Windows System**), that 
keep everyhing in order:

A simple example for today:

![Example for Today](imgs/exam-a-01.png)\

And then, open the file explorer and rename the *numbered* folders with their correct names and make his operations (Graveyard stuff).

What is our job? Create a simple SCRIPT that accepts 2 arguments :

- First argument: Number of subfolders that must be created (from 5 to 15).
- Second argument (is optional): If the second argument is present and it is `open` the script must open the FileExplorer (*thunar*, or equivalent) in the folder just created which 
  name is the current day of month.
- Second argument (is optional): If the second argument is present and it is `clean` the script delete all files and folders in the folder just created which name is the current day of month.
- Second argument (is optional): If the second argument is present and it is `extra` the script must performance the extra option (*see below*).
- If more arguments are given, or less , displays an error message and exit.
- If the optional argument is given and it is not `open` create the subfolders and display a Warning Message and the *complete Path to the Folders created*, one line for each subfolder.

\newpage
## Additional considerations:

### Testing directories

The script must create the subfolder structure : 

- `C:\Users\MYUSER\Graves\YYYY\MM-DD\`

**only if the path not exists**, if the path exists, the script must delete first the subfolders `grave-*` from inside and "re-create" them.

### Testing arguments

The subfolder number must be between 5 and 15 (both inclusive).

### Extra - PowerShell

The Script must create a copy in the Desktop folder for the Current user, that allows Max store directly the folder for today : `C:\Users\MYUSER\Graves\YYYY\MM-DD\`
.

## TASK UPLOAD

- Upload the PowerShell Script to Aules.

\newpage

# Scite and Beyond (BASH).

We need to install the Scite in several Xubuntu Machines and configure them using the configuration files that are provided from our `sysadmin` (*aka* teacher). This could be a tedious task, but we are more cleaver than our teacher expects
and we can create a Bash Script that will provide us a lot of *free-time*.

We must compare the `.SciTEUser.properties` from our Windows installation with the Xubuntu configuration provided, at the end of the task, when you complete 
the developing of the Script, you must copy if from Windows to de Xubuntu VM using the Host IP and make a port forwarding to the Xubuntu Machine SSH default port.

The Host port must be : **2822**.

Configure the NAT Network to allow this, and copy the Script to the Xubuntu Machine from your windows using `scp` as described before. Add an Screenshot in your document
of the operation completed. 

The Scite is *uploaded* to our `server` at this **URL**:

- [http://tic.ieslasenia.org/repositori/scite/scite-deb.tar.gz](http://tic.ieslasenia.org/repositori/scite/scite-deb.tar.gz)

And the configuration files are uploaded here:

- [http://tic.ieslasenia.org/repositori/scite/scite-config-linux.tar.gz](http://tic.ieslasenia.org/repositori/scite/scite-config-linux.tar.gz)

The Script must:

- Download the zip with the Installer from the server.
- Untarball-it.
- Create a TemporalFolder in our $HOME in GNU/LinuX.
- Copy the `.deb` inside.
- Install in a complete unttended way, saving the log in a file named: `YYYY-MM-DD-ExamScite.log` inside the TemporalFolder.
- Download the tar.gz with the Configuration files.
- Extract it in the TemporalFolder.
- Copy the `SciTEUser.properties` to the place that will be used by the `Scite`.
- Launch the Scite just installed.

For each Step the Script must be display a Message about the operation just performed.

## Clues

You can use : `wget` cmdlet.

## TASK UPLOAD

- Upload the BASH Script to Aules.
- Upload the document with the screenshots to Aules.








