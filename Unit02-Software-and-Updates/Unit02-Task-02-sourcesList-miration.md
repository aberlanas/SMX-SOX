---
title: Unit 02 - Sources List Migration
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOX]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Software Managers and Sources List

In order to adapt the `sources.list` to the IES La Senia Infraestructure and your Home Infraestructure, 
we need to create a Shell Script that changes the sources list mirror to the most accurate. 

The Shell Script must accepts only one *parameter* that must be or `senia` or `home` and change your 
`/etc/apt/sources.list` file to the correct ubuntu mirror repository.

At IES La Senia

```shell
deb http://tic.ieslasenia.org/ubuntu jammy main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-updates main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-security main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-backports main universe restricted multiverse
```


At Home

```shell
deb http://es.archive.ubuntu.com/ubuntu jammy main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-updates main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-security main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-backports main universe restricted multiverse
```

Additional tasks for the Script:

1. Check that the script has the root privileges.
2. Make a backup copy of the `sources.list` current file in the `/root/` directory 
with a datetime in the name, just in case.
Example: `/root/sources.list.20221011-1010.bak`
3. Make the changes.
4. Update your apt catalog.
5. Display a message.

